﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using System.IO;
using XSD_tricks.Models;

namespace XSD_tricks.Controllers
{
    public class MainController : Controller
    {
        // GET: Main
        public ActionResult Start()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Start(HttpPostedFileBase file)
        {
            if (file != null && file.ContentType == "text/xml")
            {
                XDocument xdoc = XDocument.Load(file.InputStream);

                Manager mng = new Manager();
                mng.AddNewData(xdoc);
            }
            return View();
        }

        // Демонстрация содержимого БД
        public ActionResult ShowTableData()
        {
            Manager mng = new Manager();
            if (mng.IsEmpty())
                return Content("В таблице нет данных.");
            else
            {
                var records = mng.GetAllConfigRecords(); 
                return PartialView("GetDbData", records);
            }
        }

        // Загрузка данных для полей-параметров сущности ConfigRecord
        public ActionResult ShowParameterData(long pId)
        {
            Manager mng = new Manager();
            var param = mng.GetParamData(pId);

            if (param == null)
                return Content("-");
            return PartialView("GetParamData", param);
        }
    }
}