﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace XSD_tricks.Models
{
    public class RecordContext :DbContext
    {
        public RecordContext():base("name=RecordData")
        { }

        public DbSet<ConfigRecord> ConfigRecords { get; set; }

        public DbSet<ParamRecord> ParamRecords { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ConfigRecord>()
            //            .HasRequired(m => m.ParamRecord1)
            //            .WithMany(t => t.)
            //            .HasForeignKey(m => m.HomeTeamId)
            //            .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Match>()
            //            .HasRequired(m => m.GuestTeam)
            //            .WithMany(t => t.AwayMatches)
            //            .HasForeignKey(m => m.GuestTeamId)
            //            .WillCascadeOnDelete(false);
            modelBuilder.Entity<ConfigRecord>().HasRequired(m => m.ParamRecord1).WithMany(m => m.CRCollection_Id).HasForeignKey(m => m.PersonId).WillCascadeOnDelete(false);
            modelBuilder.Entity<ConfigRecord>().HasRequired(m => m.ParamRecord2).WithMany(m => m.CRCollection_Surname).HasForeignKey(m => m.PersonSurname).WillCascadeOnDelete(false);
            modelBuilder.Entity<ConfigRecord>().HasRequired(m => m.ParamRecord3).WithMany(m => m.CRCollection_Name).HasForeignKey(m => m.PersonName).WillCascadeOnDelete(false);
            modelBuilder.Entity<ConfigRecord>().HasRequired(m => m.ParamRecord4).WithMany(m => m.CRCollection_MiddleName).HasForeignKey(m => m.PersonMiddleName).WillCascadeOnDelete(false);
            modelBuilder.Entity<ConfigRecord>().HasRequired(m => m.ParamRecord5).WithMany(m => m.CRCollection_Gender).HasForeignKey(m => m.PersonGender).WillCascadeOnDelete(false);
            modelBuilder.Entity<ConfigRecord>().HasRequired(m => m.ParamRecord6).WithMany(m => m.CRCollection_BirthDate).HasForeignKey(m => m.PersonBirthDate).WillCascadeOnDelete(false);
        }
    }
}