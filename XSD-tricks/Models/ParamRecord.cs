﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XSD_tricks.Models
{
    public class ParamRecord
    {
        [Required, Key]
        public long Id { get; set; }

        public string Value { get; set; }

        public bool IsNullable { get; set; }

        public string DictionaryName { get; set; }

        public int Length { get; set; }

        public virtual ICollection<ConfigRecord> CRCollection_Id { get; set; }
        public virtual ICollection<ConfigRecord> CRCollection_Surname { get; set; }
        public virtual ICollection<ConfigRecord> CRCollection_Name { get; set; }
        public virtual ICollection<ConfigRecord> CRCollection_MiddleName { get; set; }
        public virtual ICollection<ConfigRecord> CRCollection_Gender { get; set; }
        public virtual ICollection<ConfigRecord> CRCollection_BirthDate { get; set; }
    }
}