﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace XSD_tricks.Models
{
    public class Manager
    {
        // Проверка наличия записей в ConfigRecords
        public bool IsEmpty()
        {
            using (RecordContext rc = new RecordContext())
            {
                var result = rc.Set<ConfigRecord>().Count();

                if (result < 1)
                    return true;
                else
                    return false;
            }
        }

        // Добавление записи типа ParamRecord
        private void AddParameterRecord(ParamRecord pRec)
        {
            using (RecordContext rc = new RecordContext())
            {
                rc.Set<ParamRecord>().Add(pRec);
                rc.SaveChanges();
            }
        }

        // Добавление группы записей ParamRecord
        private void AddMultipleParameterRecords(IEnumerable<ParamRecord> pRecs)
        {
            using (RecordContext rc = new RecordContext())
            {
                rc.Set<ParamRecord>().AddRange(pRecs);
                rc.SaveChanges();
            }

        }

        // Добавление записи типа ConfigRecord
        private void AddConfigRecord(ConfigRecord cRec)
        {
            using (RecordContext rc = new RecordContext())
            {
                rc.Set<ConfigRecord>().Add(cRec);
                rc.SaveChanges();
            }
        }

        /// <summary>
        /// Создание и возврат экземпляра ParamRecord на основе данных из xml
        /// </summary>
        /// <param name="collection">Коллекция узлов, где идет поиск</param>
        /// <param name="pName">Имя узла</param>
        /// <returns></returns>
        private ParamRecord CreateParamRecordInstance(IEnumerable<XElement> collection, string pName)
        {
            var attributes = collection.Where(n => n.Name == pName).Single().Elements().Where(e => e.Name == "Validator").Single().Attributes();
            ParamRecord newParam = new ParamRecord();

            newParam.IsNullable = attributes.Where(a => a.Name == "nullable").Any() && attributes.Where(a => a.Name == "nullable").Single().Value == "true" ?
                true :
                false;

            newParam.DictionaryName = attributes.Where(a => a.Name == "dictionaryName").Any() ?
                attributes.Where(a => a.Name == "dictionaryName").Single().Value :
                "null";

            newParam.Value = attributes.Where(a => a.Name == "value").Any() ?
                attributes.Where(a => a.Name == "value").Single().Value :
                "null";

            newParam.Length = attributes.Where(a => a.Name == "length").Any() && attributes.Where(a => a.Name == "length").Single().Value == "1" ?
                1 :
                100;

            return newParam;
        }

        // Метод для парсинга xml-файла и записи результатов в БД
        public void AddNewData(XDocument xdoc)
        {
            // получение внутренней структуры для поиска данных
            var nodes = xdoc.Descendants();

            // создание экземпляра ParamRecord для каждого из узлов
            var pId = CreateParamRecordInstance(nodes, "Id");
            var pSurname = CreateParamRecordInstance(nodes, "Surname");
            var pName = CreateParamRecordInstance(nodes, "Name");
            var pMiddleName = CreateParamRecordInstance(nodes, "MiddleName");
            var pGender = CreateParamRecordInstance(nodes, "Gender");
            var pBirthDate = CreateParamRecordInstance(nodes, "BirthDate");

            // запись сущностей ParamRecords в БД
            AddMultipleParameterRecords(new List<ParamRecord>(){
                pId,
                pSurname,
                pName,
                pMiddleName,
                pGender,
                pBirthDate
            });

            // формирование экземпляра ConfigRecord
            ConfigRecord newRecord = new ConfigRecord()
            {
                ConnectionString = nodes.Where(n => n.Name == "ConnectionString").Single().Value,
                QueryString = nodes.Where(n => n.Name == "QueryString").Single().Value,
                HotelSystem = nodes.Where(n => n.Name == "HotelSystem").Single().Value,
                PersonId = pId.Id,
                PersonSurname = pSurname.Id,
                PersonName = pName.Id,
                PersonMiddleName = pMiddleName.Id,
                PersonGender = pGender.Id,
                PersonBirthDate = pBirthDate.Id
            };

            // запись сущности ConfigRecord в БД
            AddConfigRecord(newRecord);
        }

        // Получение списка всех записей таблицы ConfigRecords
        public IEnumerable<ConfigRecord> GetAllConfigRecords()
        {
            using (RecordContext rc = new RecordContext())
            {
                if (rc.ConfigRecords.Count() < 1)
                    return null;
                else
                    return rc.Set<ConfigRecord>().ToList();
            }
        }

        // Получение сущности ParamRecord по ID
        public ParamRecord GetParamData(long paramId)
        {
            using (RecordContext rc = new RecordContext())
            {
                var result = from p in rc.Set<ParamRecord>() where p.Id == paramId select p;

                if (result == null || result.Any() == false)
                    return null;
                else
                    return result.Single();
            }
        }
    }
}