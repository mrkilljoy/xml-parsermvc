﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XSD_tricks.Models
{
    public class ConfigRecord
    {
        [Required, Key]
        public long Id { get; set; }

        public string ConnectionString { get; set; }

        public string QueryString { get; set; }

        public string HotelSystem { get; set; }

        public long PersonId { get; set; }
        [ForeignKey("PersonId")]
        public virtual ParamRecord ParamRecord1 { get; set; }

        public long PersonSurname { get; set; }
        [ForeignKey("PersonSurname")]
        public virtual ParamRecord ParamRecord2 { get; set; }

        public long PersonName { get; set; }
        [ForeignKey("PersonName")]
        public virtual ParamRecord ParamRecord3 { get; set; }

        public long PersonMiddleName { get; set; }
        [ForeignKey("PersonMiddleName")]
        public virtual ParamRecord ParamRecord4 { get; set; }

        public long PersonGender { get; set; }
        [ForeignKey("PersonGender")]
        public virtual ParamRecord ParamRecord5 { get; set; }

        public long PersonBirthDate { get; set; }
        [ForeignKey("PersonBirthDate")]
        public virtual ParamRecord ParamRecord6 { get; set; }
    }
}